import Registration from './components/Forms/Registration';
import {Route,Redirect,Switch, useHistory} from 'react-router-dom';
import React from 'react';
import Home from './components/Kyathi/Home';
import Mainform from './components/Forms/Mainform';
import "./App.css";
import Videopage from './components/MainScreen/Videopage';
import firepadRef from "./server/firebase";

function App() {
  const history = useHistory();
  const firebaseid = `${`?id=`}${firepadRef.key}`;
  const searchid =window.location.search;
  const compareid = firebaseid === searchid 
      ? true
      : false;
  const loggedin = window.localStorage.getItem("Login_Info"); 
  console.log("this is search id",searchid);
  console.log("this is firebase id",firebaseid);
  if (compareid) {
    if (loggedin === "loggedin") {
      history.push("/meeting-page/" + `?id=${firepadRef.key}`);
    } else {
      history.push("/login");
    }
  } 
  
  return (
    <div className="App">
      <Route>
        <Switch>
          <Route path="/" exact>
            <Redirect to="/login" />
          </Route>
          <Route path="/login">
            <Mainform />
          </Route>
          <Route path="/Registration">
            <Registration />
          </Route>
          <Route path="/after">
            <Home />
          </Route>
          <Route path="/meeting-page">
            <Videopage />
          </Route>
        </Switch>
      </Route>
    </div>
  );
}


export default App;
