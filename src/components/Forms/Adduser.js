import './Adduser.css';
import React,{useRef,useEffect,useState} from 'react';
import ClearIcon from '@mui/icons-material/Clear';
import Mailsent from '../messages/Mailsent';
function Adduser(props){
    const [mailsent,setmailsent]=useState();
    const add=useRef();
    useEffect(()=>{
        add.current.focus();
    },[])
    const link=props.url;
    const adduserHandler=(event)=>{
      event.preventDefault();
  
      const entereduserEmail = add.current.value;
      
      
      fetch(
        "https://nikhil010.pythonanywhere.com/meeting/",
        {
          method: "POST",
          body: JSON.stringify({
           email: entereduserEmail,
           Link:link,
           returnSecureToken: true,
          }),
          headers: 
          {
            "Content-Type": "application/json",
          },
        })
        .then((res) => {
          if (res.ok) {
            setmailsent(true);
          } else {
            return res.json().then((data) => {
              let errorMessage = "Authentication failed!";
              if (data && data.error && data.error.message) {
                errorMessage = data.error.message;
              }

              throw new Error(errorMessage);
            });
          }
        })
        .then((data) => {
          console.log(data);
        })
    }
    const closemailhandler=()=>{
      setmailsent(false);
    }
    return (
      <div className="addbackdrop">
        <div className="add">
          <form onSubmit={adduserHandler}>
            <div className='adduser'>
              <h1>Add People</h1>
              <ClearIcon
                style={{
                  marginLeft: "240px",
                  marginTop: "30px",
                  cursor: "pointer",
                }}
                onClick={props.closeadd}
              />
            </div>
            <input className='adduser-input' type="email" placeholder="Enter email" ref={add} />
            <button className="sbtn" type="submit">
              Send Mail
            </button>
          </form>
        </div>
        {mailsent && <Mailsent closemail={closemailhandler}/>}
      </div>
    );
}
export default Adduser;